export class Order {
    id: number;
    bookIntegerMap: Map<number, number>;
    status: String;
}
