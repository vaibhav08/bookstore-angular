import { Component, OnInit } from '@angular/core';
import { ActivatedRoute , Router } from '@angular/router';
import { CartService } from '../../services/cart.service';
import { BookService } from '../../services/book.service';
import { Book } from 'src/app/models/book';
import { THIS_EXPR, ThrowStmt } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  bookQuantityMap: Map<number , number>;
  books: Book[] = [];
  empty = false;
  private customerId: number;
  constructor(private route: ActivatedRoute , private router: Router,
    private cartService: CartService, private bookService: BookService) { }

  ngOnInit() {
    this.customerId = this.route.snapshot.params['customerId'];
    this.cartService.getCartItems(this.customerId).subscribe(
      data => { this.bookQuantityMap = data; },
      err => {},
      () => { if (Object.keys(this.bookQuantityMap).length === 0 ) {
        this.empty = true;
      }
      console.log(this.bookQuantityMap);
        Array.from(Object.keys(this.bookQuantityMap)).forEach(
        key => { this.bookService.getBookById(key).subscribe(
          data => { this.books.push(data); },
          err => { console.log(err); },
          () => { if (this.books.length === 0 ) {
            console.log('book legnth' + this.books.length);
            this.empty = true;
          }}
        ); }
      ); }
    );
  }
  removeFromCart(bookId: number) {
    console.log(bookId);
    this.cartService.deleteBookFromCart(this.customerId, bookId).subscribe(
      data => { console.log(data); },
      err => { console.log(err); },
      () => { window.location.reload(); }
    );
  }
  decreaseQuantity(bookId: number) {
    this.cartService.reduceBookFromCart(this.customerId, bookId).subscribe(
      data => { console.log(data); },
      err => { console.log(err); },
      () => { window.location.reload(); }
    );
  }
  increaseQuantity(bookId: number) {
    this.cartService.addBookToCart(this.customerId, bookId).subscribe(
      data => { console.log(data); },
      err => { console.log(err); },
      () => { window.location.reload(); }
    );
  }

  goShopping() {
    this.router.navigate(['/dashboard', this.customerId]);
  }

  makeOrder() {
    this.cartService.makeOrder(this.customerId).subscribe(
      data => {console.log(data); },
      err => { console.log(err); },
      () => { this.router.navigate(['/order-details', this.customerId]); }
    );
  }
}
