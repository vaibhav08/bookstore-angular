import { Component, OnInit } from '@angular/core';
import { ActivatedRoute , Router } from '@angular/router';
import { CustomerService } from '../../services/customer.service';
import { BookService } from '../../services/book.service';
import { Customer } from '../../models/customer';
import { Book } from '../../models/book';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  customerId: number;
  customer: Customer;
  books: Book[];
  constructor(private route: ActivatedRoute, private router: Router,
     private customerService: CustomerService, private bookService: BookService) { }

  ngOnInit() {
    this.customerId = this.route.snapshot.params['customerId'];
    this.customerService.getExistingCustomerFromId(this.customerId).subscribe(
      data => { this.customer = data; },
      err => { console.log(err); },
      () => { console.log('customer loaded'); }
    );
    this.bookService.getAllBooks().subscribe(
      data => { this.books = data ; },
      err => { console.log(err); },
      () => { console.log('done loading books'); }
    );
  }

  goToDetail(bookId: number) {
    this.router.navigate(['/book-detail', this.customerId, bookId]);
  }

}
