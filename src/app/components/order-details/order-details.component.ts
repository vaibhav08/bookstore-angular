import { Component, OnInit } from '@angular/core';
import { ActivatedRoute , Router } from '@angular/router';
import { Order } from '../../models/order';
import { OrderService } from '../../services/order.service';
import { BookService } from '../../services/book.service';
import { Book } from '../../models/book';
@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {
  orders: Order[];
  books: Book[] = [];
  noOrder = false;
  bookIntegerMap: Map<number, number>;
  customerId: number;
  constructor(private route: ActivatedRoute, private router: Router,
     private orderService: OrderService, private bookService: BookService) { }

  ngOnInit() {
    this.customerId = this.route.snapshot.params['customerId'];
    this.orderService.getCustomerOrders(this.customerId).subscribe(
      data => { this.orders = data; },
      err => { console.log(err); },
      () => {
              if (this.orders.length === 0) {
                this.noOrder = true;
               }
      }
    );
  }
}
