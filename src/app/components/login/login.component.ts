import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CustomerService } from '../../services/customer.service';
import { LoginEntity } from '../../models/loginEntity';
import { Router } from '@angular/router';
import { Customer } from 'src/app/models/customer';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  // mail: String = '';
  loginForm = new FormGroup({
    email : new FormControl(''),
    password : new FormControl('')
  });

  login = false;
  private customer: Customer;
  constructor(private customerService: CustomerService , private router: Router) { }

  onSubmit() {
    const loginEntity: LoginEntity = {
      email: this.loginForm.value.email,
      password: this.loginForm.value.password
    };
    this.customerService.getCustomerIfExist(loginEntity).subscribe(
      data => { this.customer = data; console.log(this.customer.id); } ,
      err =>  console.log(err)  ,
      () => { if (this.customer != null) {
        this.router.navigate(['/dashboard', this.customer.id]);
      } else {
        this.login = false;
        alert('Invalid Email or Password!');
      } }
    );
  }
  ngOnInit() {
  }

}
