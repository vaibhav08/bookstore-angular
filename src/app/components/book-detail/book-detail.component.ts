import { Component, OnInit, Input } from '@angular/core';
import { Book } from 'src/app/models/book';
import { ActivatedRoute , Router} from '@angular/router';
import { BookService } from '../../services/book.service';
import { CartService } from '../../services/cart.service';
@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit {
  private count = 0;
  private visible = false;
  private book: Book;
  private customerId: number;
  private bookId: number;
  constructor(private route: ActivatedRoute, private router: Router,
    private bookService: BookService, private cartService: CartService) { }

  ngOnInit() {
    this.bookId = this.route.snapshot.params['bookId'];
    this.customerId = this.route.snapshot.params['customerId'];
    this.bookService.getBookById(this.bookId).subscribe(
      data => { this.book = data; },
      err => { console.log(err); },
      () => { console.log('done loading book'); }
    );
  }
  increaseCount() {
    this.count++;
    this.cartService.addBookToCart(this.customerId, this.bookId).subscribe(
      data => { console.log(data.msg); },
      err => { console.log(err); },
      () => {console.log('book added to cart'); }
    );
  }
  decreaseCount() {
    if (this.count > 0) {
      this.count--;
      this.cartService.reduceBookFromCart(this.customerId, this.bookId).subscribe(
        data => { console.log(data.msg); },
        err => { console.log(err); },
        () => { console.log('book removed from cart'); }
      );
    }
  }
  goToCart() {
    this.router.navigate(['/cart', this.customerId]);
  }
}
