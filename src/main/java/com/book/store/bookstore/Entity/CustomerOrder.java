package com.book.store.bookstore.Entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;
@Entity
@Data
public class CustomerOrder {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private int id;
   // private LocalDateTime localDateTime;
    @ElementCollection
    @Column(name="value")
    @CollectionTable(name="cartToOrder", joinColumns=@JoinColumn(name="cust_id"))
    private Map<Integer,Integer> bookIntegerMap=new HashMap<>();
    private String status;
}

